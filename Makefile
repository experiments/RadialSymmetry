all:
	./RadialSymmetry.py

test:
	./RadialSymmetryDiagram.py

pep8:
	pep8 --ignore=E501 *.py

clean:
	rm -f *.pyc radial_symmetry_test.svg radial_symmetry_test.png
