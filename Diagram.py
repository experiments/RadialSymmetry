#!/usr/bin/env python
#
# A Diagram abstraction based on Cairo
#
# Copyright (C) 2015  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import cairo
from math import *


class Diagram(object):

    def __init__(self, width, height, background=[1, 1, 1], font_size=20):
        self.width = width
        self.height = height
        self.background = background

        # TODO: use a RecordingSurface
        self.surface = cairo.SVGSurface(None, width, height)
        self.cr = cr = cairo.Context(self.surface)

        # convert to left-bottom-origin cartesian coordinates
        cr.translate(0, self.height)
        cr.scale(1, -1)

        cr.select_font_face("Georgia", cairo.FONT_SLANT_NORMAL,
                            cairo.FONT_WEIGHT_NORMAL)
        cr.set_font_size(font_size)

        # Adjust the font matrix to left-bottom origin
        M = cr.get_font_matrix()
        M.scale(1, -1)
        cr.set_font_matrix(M)

    def clear(self):
        cr = self.cr

        r, g, b, a = self.color_to_rgba(self.background)
        cr.set_source_rgba(r, g, b, a)
        cr.paint()

    def save_svg(self, filename):
        surface = cairo.SVGSurface(filename + '.svg', self.width, self.height)
        cr = cairo.Context(surface)
        cr.set_source_surface(self.surface, 0, 0)
        cr.paint()

    def save_png(self, filename):
        surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, self.width, self.height)
        cr = cairo.Context(surface)
        cr.set_source_surface(self.surface, 0, 0)
        cr.paint()
        surface.write_to_png(filename + '.png')

    def show(self):
        import Image
        import StringIO
        f = StringIO.StringIO()
        surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, self.width, self.height)
        cr = cairo.Context(surface)
        cr.set_source_surface(self.surface, 0, 0)
        cr.paint()
        surface.write_to_png(f)
        f.seek(0)
        im = Image.open(f)
        im.show()

    def get_regular_polygon(self, x, y, sides, r, theta0=0.0):
        theta = 2 * pi / sides

        verts = []
        for i in range(0, sides):
            px = x + r * sin(theta0 + i * theta)
            py = y + r * cos(theta0 + i * theta)
            verts.append((px, py))

        return verts

    def color_to_rgba(self, color):
        if len(color) == 3:
            return color[0], color[1], color[2], 1.0
        elif len(color) == 4:
            return color[0], color[1], color[2], color[3]
        else:
            return None

    def _draw_polygon(self, verts):
        cr = self.cr

        v = verts[0]
        cr.move_to(v[0], v[1])
        for v in verts[1:]:
            cr.line_to(v[0], v[1])
        cr.close_path()

    def draw_polygon_by_verts(self, verts, fill_color=None, stroke_color=[0, 0, 0]):
        cr = self.cr

        if fill_color:
            self._draw_polygon(verts)
            r, g, b, a = self.color_to_rgba(fill_color)
            cr.set_source_rgba(r, g, b, a)
            cr.fill()

        if stroke_color:
            self._draw_polygon(verts)
            r, g, b, a = self.color_to_rgba(stroke_color)
            cr.set_source_rgba(r, g, b, a)
            cr.stroke()

    def draw_polygon(self, cx, cy, sides, r, theta=0.0, fill_color=None, stroke_color=[0, 0, 0]):
        verts = self.get_regular_polygon(cx, cy, sides, r, theta)
        self.draw_polygon_by_verts(verts, fill_color, stroke_color)
        return verts

    def draw_star_by_verts(self, cx, cy, verts, stroke_color=[0, 0, 0]):
        cr = self.cr

        for v in verts:
            cr.move_to(cx, cy)
            cr.line_to(v[0], v[1])

        r, g, b, a = self.color_to_rgba(stroke_color)
        cr.set_source_rgba(r, g, b, a)
        cr.stroke()

    def draw_star(self, cx, cy, sides, r, theta=0.0, stroke_color=[0, 0, 0]):
            apothem = r * cos(pi / sides)
            apothem_angle = theta + pi / sides

            verts = self.get_regular_polygon(cx, cy, sides, apothem, apothem_angle)
            self.draw_star_by_verts(cx, cy, verts, stroke_color)
            return verts

    def draw_circle(self, cx, cy, size=10.0, fill_color=[0, 0, 0, 0.5],
                    stroke_color=None):
        cr = self.cr

        cr.save()
        cr.arc(cx, cy, size, 0, 2 * pi)

        if fill_color:
            r, g, b, a = self.color_to_rgba(fill_color)
            cr.set_source_rgba(r, g, b, a)
            cr.fill()

        if stroke_color:
            r, g, b, a = self.color_to_rgba(stroke_color)
            cr.set_source_rgba(r, g, b, a)
            cr.stroke()

        cr.restore()

    def normalized_angle_01(self, theta):
        return fmod(theta, 2 * pi) / (2 * pi)

    def draw_line(self, x1, y1, x2, y2, stroke_color=[0, 0, 0, 1]):
        cr = self.cr
        cr.move_to(x1, y1)
        cr.line_to(x2, y2)
        r, g, b, a = self.color_to_rgba(stroke_color)
        cr.set_source_rgba(r, g, b, a)
        cr.stroke()

    def draw_rect_from_center(self, cx, cy, width, height, theta=0,
                              fill_color=[1, 1, 1, 0.8], stroke_color=None):
        cr = self.cr

        # the position of the center of a rectangle at (0,0)
        mx = width / 2.0
        my = height / 2.0

        # calculate the position of the bottom-left corner after rotating the
        # rectangle around the center
        rx = cx - (mx * cos(theta) - my * sin(theta))
        ry = cy - (mx * sin(theta) + my * cos(theta))

        self.draw_rect(rx, ry, width, height, theta, fill_color, stroke_color)

    def draw_rect(self, x, y, width, height, theta=0,
                  fill_color=[1, 1, 1, 0.8], stroke_color=None):
        cr = self.cr

        cr.save()
        cr.translate(x, y)
        cr.rotate(theta)

        if fill_color:
            cr.rectangle(0, 0, width, height)
            r, g, b, a = self.color_to_rgba(fill_color)
            cr.set_source_rgba(r, g, b, a)
            cr.fill()

        if stroke_color:
            cr.rectangle(0, 0, width, height)
            r, g, b, a = self.color_to_rgba(stroke_color)
            cr.set_source_rgba(r, g, b, a)
            cr.stroke()

        cr.restore()

    def draw_centered_text(self, cx, cy, text, theta=0,
                           color=[0, 0, 0],
                           align_baseline=False,
                           bb_fill_color=[1, 1, 1, 0.8],
                           bb_stroke_color=None):
        cr = self.cr

        x_bearing, y_bearing, width, height, x_advance = cr.text_extents(text)[:5]
        ascent, descent = cr.font_extents()[:2]

        # The offset of the lower-left corner of the text.
        tx = width / 2.0 + x_bearing

        if align_baseline:
            # When aligning to the  baseline it is convenient the make the
            # bounding box depend on the font vertical extent and not from the
            # text content.
            ty = 0
            bb = [0, -descent, width, ascent]
        else:
            ty = height / 2.0 + y_bearing
            bb = [0, y_bearing, width, height]

        # Angles are intended clockwise by the caller, but the trigonometric
        # functions below consider angles counter-clockwise
        theta = -theta

        # The coordinate of the lower-left corner of the rotated rectangle
        rx = cx - tx * cos(theta) + ty * sin(theta)
        ry = cy - tx * sin(theta) - ty * cos(theta)

        cr.save()
        cr.translate(rx, ry)
        cr.rotate(theta)

        if bb_fill_color or bb_stroke_color:
            self.draw_rect(bb[0], bb[1], bb[2], bb[3], 0, bb_fill_color, bb_stroke_color)

        r, g, b, a = self.color_to_rgba(color)
        cr.set_source_rgba(r, g, b, a)
        cr.move_to(0, 0)
        cr.show_text(text)
        cr.fill()
        cr.restore()

        return x_advance


if __name__ == "__main__":
    diagram = Diagram(400, 400)

    diagram.clear()

    x = 40
    y = 200

    x_offset = x

    theta = 0

    advance = diagram.draw_centered_text(x_offset, y, "Ciao", theta, align_baseline=True, bb_stroke_color=[0, 0, 0, 0.5])
    x_offset += advance

    advance = diagram.draw_centered_text(x_offset, y, "____", theta + pi / 4, align_baseline=True, bb_stroke_color=[0, 0, 0, 0.5])
    x_offset += advance

    advance = diagram.draw_centered_text(x_offset, y, "jxpqdlf", theta + pi / 2, align_baseline=True, bb_stroke_color=[0, 0, 0, 0.5])
    x_offset += advance

    advance = diagram.draw_centered_text(x_offset, y, "pppp", theta + 3 * pi / 4, align_baseline=True, bb_stroke_color=[0, 0, 0, 0.5])
    x_offset += advance

    advance = diagram.draw_centered_text(x_offset, y, "dddd", theta + pi, align_baseline=True, bb_stroke_color=[0, 0, 0, 0.5])
    x_offset += advance

    advance = diagram.draw_centered_text(x_offset, y, "Jjjj", theta + 5 * pi / 4, align_baseline=True, bb_stroke_color=[0, 0, 0, 0.5])
    x_offset += advance

    advance = diagram.draw_centered_text(x_offset, y, "1369", theta + 3 * pi / 2, align_baseline=True, bb_stroke_color=[0, 0, 0, 0.5])
    x_offset += advance

    advance = diagram.draw_centered_text(x_offset, y, "qqqq", theta + 7 * pi / 4, align_baseline=True, bb_stroke_color=[0, 0, 0, 0.5])
    x_offset += advance

    diagram.draw_line(0, y, 400, y, [0, 0, 1, 0.2])

    diagram.draw_rect(40, 40, 300, 100, stroke_color=[0, 0, 0, 0.8])
    diagram.draw_rect(40, 40, 300, 100, pi / 30, stroke_color=[0, 0, 0, 0.8])

    diagram.draw_rect(40, 250, 300, 100, stroke_color=[0, 0, 0, 0.8])
    diagram.draw_rect_from_center(40 + 150, 250 + 50, 300, 100, theta=(pi / 40), stroke_color=[1, 0, 0], fill_color=None)

    diagram.show()
